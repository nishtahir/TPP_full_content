# What?

The full text of all TPP documents, concatenated using `pdfunite`, found in `FULL_CONTENTS_TPP.pdf`

# Why?

I realized how annoying it would be to pull down every item available for the TPP individually, so I designed a document query to grab all of the URLs and then used wget to download them.

# How?

Go here in your browser https://ustr.gov/trade-agreements/free-trade-agreements/trans-pacific-partnership/tpp-full-text

open the debug toolkit of whatever browser it is, then enter the function found in `url_list.js`

The output of this command can be found in `pdf_list.txt`
